import 'package:di_practice/infrastructure/injection.config.dart';
import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';

final getIt = GetIt.instance;
@InjectableInit()
void configDependencieInjection(String environment)=> $initGetIt(getIt,environment: environment);

abstract class Env{
  static const String dev = "dev";
  static const String prod = "prod";
}