import 'package:di_practice/presentation/counter_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CounterUi extends StatefulWidget {
  CounterUi({Key key}) : super(key: key);

  @override
  _CounterUiState createState() => _CounterUiState();
}

class _CounterUiState extends State<CounterUi> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("done with it"),
      ),
      body: Consumer<counterChangeNotifier>(
        builder: (context, counter, child) {
          return Center(
            child: Text(
              ' the actual counter is: ${counter.value}',
            ),
          );
        },
      ) ,
      floatingActionButton: FloatingActionButton(
        onPressed: Provider.of<counterChangeNotifier>(context).increment,
        child: Icon(Icons.add,color: Colors.white,),
      ),
    );
  }
}