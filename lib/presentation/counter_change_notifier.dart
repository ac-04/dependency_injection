
import 'package:di_practice/domain/IRepository.dart';
import 'package:flutter/cupertino.dart';
import 'package:injectable/injectable.dart';
@Injectable()
class counterChangeNotifier extends ChangeNotifier{

  final IRepository repo;

  counterChangeNotifier(this.repo);

  int _value = 0;
  int get value => _value;

  void increment() {
    _value += this.repo.getIncrements();
    notifyListeners();
  }
}