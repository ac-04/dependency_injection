import 'package:di_practice/domain/IRepository.dart';
import 'package:injectable/injectable.dart';

@Injectable(as: IRepository,env: ["dev"])
class devRepository implements IRepository{
  @override
  int getIncrements()=>2;
}