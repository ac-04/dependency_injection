import 'package:di_practice/infrastructure/injection.dart';
import 'package:di_practice/presentation/counter_change_notifier.dart';
import 'package:di_practice/presentation/counter_ui.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

main(){
  configDependencieInjection(Env.prod);
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Change notifier with dependencie injection",
      home: ChangeNotifierProvider(
        create: (_) => getIt<counterChangeNotifier>(),
        child: CounterUi(),
      ),
    );
  }
}
