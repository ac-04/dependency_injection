import 'package:di_practice/domain/IRepository.dart';
import 'package:injectable/injectable.dart';

@Injectable(as: IRepository,env: ["prod"])
class prodRepository implements IRepository{
  @override
  int getIncrements() => 1;

}