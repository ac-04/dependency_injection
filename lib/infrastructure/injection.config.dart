// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';

import '../domain/IRepository.dart';
import '../presentation/counter_change_notifier.dart';
import 'devRepository.dart';
import 'prodRepository.dart';

/// Environment names
const _prod = 'prod';
const _dev = 'dev';

/// adds generated dependencies
/// to the provided [GetIt] instance

GetIt $initGetIt(
  GetIt get, {
  String environment,
  EnvironmentFilter environmentFilter,
}) {
  final gh = GetItHelper(get, environment, environmentFilter);
  gh.factory<IRepository>(() => prodRepository(), registerFor: {_prod});
  gh.factory<IRepository>(() => devRepository(), registerFor: {_dev});
  gh.factory<counterChangeNotifier>(
      () => counterChangeNotifier(get<IRepository>()));
  return get;
}
